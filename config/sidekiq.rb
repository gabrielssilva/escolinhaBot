require 'sidekiq'

Sidekiq.configure_client do |sidekiq_config|
  sidekiq_config.redis = { url: Escolinha.sidekiq_redis_url }
end

Sidekiq.configure_server do |sidekiq_config|
  sidekiq_config.redis = { url: Escolinha.sidekiq_redis_url }
end

# Load all worker classes.
Dir[File.join(Escolinha::ROOT, 'workers', '*')].each { |file| require file }
