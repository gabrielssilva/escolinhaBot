class UpdatesBotWorker
  include Sidekiq::Worker

  def perform
    store = Store.new
    crawler = Crawler.new(Escolinha.api_url('courses'), store.saved_courses)
    courses = crawler.new_courses
    return if courses.empty?

    # So we can append different SocialApi::Base extensions
    [SocialApi::Twitter].each do |klass|
      api = klass.new(Escolinha::CONF[klass.conf_key], Escolinha.platform_url)
      courses.each{ |course| api.publish_course(course) }
    end
    store.save_courses(courses)
  end
end
