module SocialApi
  class Base
    attr_reader :client

    def initialize(config, platform_url)
      @client = self.register(config)
      @platform_url = platform_url
    end

    def register(config)
      # Returns a registered instance of the API client
      raise 'not implemented'
    end

    def publish_course(course)
      # Publishes a new post regarding the course publication
      raise 'not implemented'
    end
  end
end
