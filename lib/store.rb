class Store
  def initialize
    @redis = Redis.new(url: Escolinha.store_redis_url)
  end

  def saved_courses
    @redis.smembers('course:ids')
  end

  def save_courses(courses)
    course_ids = courses.map{ |course| course['course_id'] }
    @redis.sadd('course:ids', course_ids)
  end
end
