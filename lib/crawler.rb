require 'net/http'
require 'json'

class Crawler
  def initialize(api_url, existent_ids)
    @uri = URI(api_url)
    @existent_ids = existent_ids.map{ |id| id.to_s }
  end

  def new_courses
    fetch_courses.select{ |c| !@existent_ids.include?(c['course_id'].to_s) }
  end

  private

  def fetch_courses
    response = Net::HTTP.get(@uri)
    JSON.parse(response)
  end
end
