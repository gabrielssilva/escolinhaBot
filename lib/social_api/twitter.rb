require 'twitter'
require_relative '../social_api'

class SocialApi::Twitter < SocialApi::Base
  def register(config)
    ::Twitter::REST::Client.new do |twitter_config|
      twitter_config.consumer_key        = config['consumer_key']
      twitter_config.consumer_secret     = config['consumer_secret']
      twitter_config.access_token        = config['access_token']
      twitter_config.access_token_secret = config['access_secret']
    end
  end

  def publish_course(course)
    message = generate_message(course)
    client.update(message)
  end

  def self.conf_key
    'twitter'
  end

  private

  def messages
    [
      "O curso %{title} acabou de ser publicado. Veja mais em %{url}.",
      "Olha! %{title} foi publicado. Acesse %{url} pra começar a aprender.",
      "Curso novo! Aprenda com o %{title}, é só acessar %{url}.",
      "Mais um curso, %{title}. Confere lá em %{url}."
    ]
  end

  def generate_message(course)
    messages.sample % { title: course['title'], url: @platform_url }
  end
end
