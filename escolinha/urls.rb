module Escolinha::Urls
  def redis_url(database)
    config = Escolinha::CONF['redis']
    "redis://#{config['host']}:#{config['port']}/#{database}"
  end

  def sidekiq_redis_url
    redis_url(Escolinha::CONF['sidekiq']['redis']['db'])
  end

  def store_redis_url
    redis_url(Escolinha::CONF['store']['redis']['db'])
  end

  def platform_url
    Escolinha::CONF['eadbox']['platform_url']
  end

  def api_url(endpoint)
    base_url = Escolinha::CONF['eadbox']['api_url']
    "#{base_url.sub(/\/$/, '')}/#{endpoint.sub(/^\//, '')}"
  end

end
