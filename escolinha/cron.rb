require 'sidekiq-cron'

module Escolinha::Cron
  JOB_NAME = 'Updates Bot'

  def schedule_bot!
    Sidekiq::Cron::Job.create(name: JOB_NAME, cron: '*/30 * * * *',
                              class: 'UpdatesBotWorker')
  end

  def stop_bot!
    Sidekiq::Cron::Job.destroy(JOB_NAME)
  end

end
