# Sobre o projeto

Um bot para notificação de novos cursos em redes sociais.

## Sobre o Bot

Este bot consiste em um worker do Sidekiq que é agendado para execução a cada 30 minutos. Em cada execução, o bot recupera os cursos através da API, e envia uma notificação para cada curso novo. Uma base de dados do Redis é usada como referência de quais cursos já foram notificados.

Por mais que a API ofereça um webhook para o evento de criação de cursos, a utilização depende de aprovação prévia do suporte, o que justificou a decisão de utilizar um crawler. No caso de utilizar o webhook, a estratégia seria impelemntar um endpoint com o Sinatra para receber as notificações. Com exceção do worker, o restante do código ainda seria o mesmo.

É importante notar que a API retorna elementos paginados, mas não inclui os [metadados de paginação](https://developer.github.com/v3/guides/traversing-with-pagination/) no Header da resposta, o que facilitaria a navegação do crawler. Neste caso, a decisão foi considerar apenas a primeira página retornada pela API.

## Estrutura do projeto

O projeto contém um módulo principal `Escolinha`, que engloba as configurações e carrega o restante do código do projeto. O diretório `/config` contém os arquivos de inicialização dos serviços, incluindo o Sidekiq. O diretório `/worker` contém as classes que serão carregadas e executadas no Sidekiq, incluindo o bot. O diretório `/lib` contém o restante da regras de negócio e demais utilitários, incluindo um wrapper para utilização do Redis e os clientes das APIs das redes sociais.

Em relação à API das redes sociais, uma infraestrutura foi desenvolvida para que novas redes sociais possam facilmente ser incluídas ao Bot. Por enquanto, apenas o Twitter é suportado.

# Dependências

* Ruby `~> 2.4.1`
* Sidekiq `~> 5.0.1`
* Redis `~> 4.0.6`
* Docker (recomendado)


# Instalação

1. Este projeto depende de um servidor Redis configurado. A forma mais rápida é utilizar uma imagem do Docker previamente configurada.

    ```
    $ docker run -p 6379:6379 -d redis
    ```

2. Instale as dependências do projeto, o que inclui o Sidekiq. Da raiz do projeto, execute:

    ```
    $ bundle install --without test
    ```

3. Crie um novo App na [página de desenvolvedores do Twitter](https://apps.twitter.com/), e insira as credenciais no arquivo `config.yml`.

4. Altere o endereço, a porta, e as bases de dados do Redis que serão usadas pelo Sidekiq e pela aplicação no arquivo `config.yml`, se necessário.

5. Altere o endereço da API e da plataforma no arquivo `config.yml`, se necessário.


# Executando o Bot

1. Caso o Redis não esteja em execução, inicie uma nova instância. Note que este comando executa um container que não persiste os dados, que não serão conservados se o container for finalizado e em iniciado novamente. Para preservar os dados, utilize um volume do Docker conforme descrito na [documentação](https://hub.docker.com/_/redis/).

    ```
    $ docker run -p 6379:6379 -d redis
    ```

2. Inicie uma instância do servidor do Sidekiq. Da raiz do projeto, execute o comando abaixo. Este comando já programa a execução do Bot.

    ```
    $ bundle exec sidekiq -r ./escolinha.rb
    ```


## Interrompendo a execução do Bot

O bot consiste em um worker do Sidekiq programado a cada 30 minutos em um cronjob. Para interromper a sua execução depois da criação, abra um novo console do ruby a partir da raiz do projeto.

```
$ irb
ruby > require_relative 'escolinha'
ruby > Escolinha.stop_bot!
```

Os comandos acima irão remover o bot do crontab. Para agendá-lo novamente, basta executar o arquivo `escolinha.rb` novamente, ou apenas reiniciar o Sidekiq com o comando direcionado acima.


# Executando os testes

1. Instale as dependências de teste

    ```
    $ bundle install
    ```
