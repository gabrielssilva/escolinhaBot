require 'yaml'

module Escolinha

  ROOT = __dir__
  Dir.glob([
    File.join(ROOT, 'escolinha', '**', '*.rb'),
    File.join(ROOT, 'lib', '**', '*.rb'),
  ]).each { |file| require file }

  # Let things break if config doesn't exist.
  CONF = YAML.load_file('config.yml')

  # Include core modules
  extend Escolinha::Urls
  extend Escolinha::Cron

  # Require conf files
  Dir[File.join(ROOT, 'config', '*')].each { |file| require file }

  begin
    schedule_bot! unless ENV['RUBY_ENV'] == 'test'
  rescue Redis::CannotConnectError
    puts '[Error] Not possible to connect to Redis. The bot was not scheduled.'
  end

end
