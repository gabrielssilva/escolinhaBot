require_relative '../../escolinha'

RSpec.describe UpdatesBotWorker do
  before(:each) { allow(Redis).to receive(:new) }

  let(:courses) { [{ 'course_id' => 1 }, { 'course_id' => 2 }] }
  let(:worker) { UpdatesBotWorker.new }

  describe '#perform' do
    context 'there are no courses previously saved' do
      before(:each) do
        expect_any_instance_of(Store).to receive(:saved_courses) { [] }
      end

      it 'posts a tweet for every new course if some courses are fetched'  do
        expect_any_instance_of(Crawler).to receive(:fetch_courses) { courses }
        expect_any_instance_of(Store).to receive(:save_courses).with(courses)
        expect_any_instance_of(Twitter::REST::Client).to receive(:update).twice
        worker.perform
      end

      it 'does not post or save anything if no courses are fetched' do
        expect_any_instance_of(Crawler).to receive(:fetch_courses) { [] }
        expect_any_instance_of(Store).not_to receive(:save_courses)
        expect_any_instance_of(Twitter::REST::Client).not_to receive(:update)
        worker.perform
      end
    end

    context 'courses were previously fetched' do
      it 'post a tweet if a new course was fetched' do
        expect_any_instance_of(Store).to receive(:saved_courses) { ['1'] }
        expect_any_instance_of(Crawler).to receive(:fetch_courses) { courses }
        expect_any_instance_of(Store).to receive(:save_courses)
                                         .with([courses.last])
        expect_any_instance_of(Twitter::REST::Client).to receive(:update).once
        worker.perform
      end

      it 'does not post or save anythong if nothing new course is fetched' do
        expect_any_instance_of(Store).to receive(:saved_courses) { ['1', '2'] }
        expect_any_instance_of(Crawler).to receive(:fetch_courses) { courses }
        expect_any_instance_of(Store).not_to receive(:save_courses)
        expect_any_instance_of(Twitter::REST::Client).not_to receive(:update)
        worker.perform
      end
    end
  end
end
