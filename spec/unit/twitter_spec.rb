require 'social_api'
require 'social_api/twitter'

RSpec.describe SocialApi::Twitter do
  let(:twitter_api) { SocialApi::Twitter.new({}, 'http://some.url') }

  describe '#messages' do
    it 'returns messages that interpolate correctly' do
      messages = twitter_api.send(:messages)
      expect do
        messages.each{ |msg| msg % { title: 'title', url: 'url' } }
      end.to_not raise_error
    end
  end

  describe '#generate_message' do
    it 'includes the course title in the generated message' do
      msg = twitter_api.send(:generate_message, { 'title' => 'Foo Bar' })
      expect(msg).to include('Foo Bar')
    end

    it 'includes the platform url in the generated message' do
      msg = twitter_api.send(:generate_message, { 'title' => 'Foo Bar' })
      expect(msg).to include('http://some.url')
    end
  end
end
