require 'crawler'

RSpec.describe Crawler do
  let(:courses) { [{ 'course_id' => 1 }, { 'course_id' => 2 }] }

  describe '#new_courses' do
    let(:crawler) { Crawler.new('http://api.url.com', []) }

    context 'with no existent courses' do
      it 'returns all fetched courses' do
        expect(crawler).to receive(:fetch_courses) { courses }
        expect(crawler.new_courses).to eq(courses)
      end
    end

    context 'with existent courses' do
      let(:crawler) { Crawler.new('http://api.url.com', ['1']) }

      it 'filters the fetched courses with the existent ones' do
        expect(crawler).to receive(:fetch_courses) { courses }
        expect(crawler.new_courses).to eq([{ 'course_id' => 2 }])
      end
    end
  end

end
